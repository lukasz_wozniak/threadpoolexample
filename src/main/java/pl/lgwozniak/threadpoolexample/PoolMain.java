package pl.lgwozniak.threadpoolexample;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * Klass represents common timer task
 * @author Łukasz Woźniak <http://lukasz-wozniak.pl>
 */
public class PoolMain extends TimerTask {

    public static final Logger log = LogManager.getLogger(PoolMain.class);

    public static void main(String[] args){
        Timer t = new Timer("PoolTimer", false);
        t.schedule(new PoolMain(), Calendar.getInstance().getTime(), 20*1000);
    }

    @Override
    public void run() {
        try{
            List<String> pools = getQueue();
            ExecutorService executors = getExecutor();
            for (String s : pools) {
                executors.execute(new Runnable(){

                    public void run()
                    {
                        parsePool(s);
                    }
                });
            }
        }catch(InterruptedException e){
            log.error("", e);
        }
    }

    private ExecutorService getExecutor() throws InterruptedException {
        ExecutorService executors = Executors.newFixedThreadPool(20, new PoolThreadFactory());
        executors.awaitTermination(6000, TimeUnit.MILLISECONDS);

        return executors;
    }

    public static List<String> getQueue(){
        List<String> pools = new ArrayList<String>();

        for(int i = 0; i < 100;  i++){
            pools.add("Pool " + i);
        }

        return pools;
    }

    public void parsePool(String s)
    {
        log.info("parsing " + s);
        try{ Thread.sleep(1000L); } catch (InterruptedException ex){}
    }

    class PoolThreadFactory implements ThreadFactory {
        private int counter = 0;
        private static final String PREFIX = "POOL-EXAMPLE-THREAD-";

        public Thread newThread(Runnable r)
        {
            return new Thread(r, new StringBuilder(PREFIX).append(counter++).toString());
        }

    }
}
